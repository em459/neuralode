import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf
from timestepper import *

tf.keras.backend.set_floatx('float64')

class Quadratic(tf.keras.layers.Layer):
    '''Quadratic form, for testing'''
    def __init__(self):
        super().__init__(self)
        A = ((1.0, 2.5, 3.1, 4.5),
             (2.5, 5.7, 6.8, 9.2),
             (3.1, 6.8, 7.1, 0.9),
             (4.5, 9.2, 0.9, 5.9))
        self.w = self.add_weight(shape=(4,4),
                                 #initializer="random_normal",
                                 initializer=tf.keras.initializers.Constant(A),
                                 trainable=True)

    def call(self,inputs):
        return tf.linalg.diag_part(tf.matmul(tf.matmul(inputs, self.w),
                                             inputs,
                                             transpose_b=True))
        

class FallingParticleLagrangian(tf.keras.layers.Layer):
    ''' Lagrangian for falling particle

    L = -q_1 + 1/2*((\dot{q}_1)^2 + (\dot{q}_2)^2)

    '''
    def __init__(self):
        super().__init__(self)

    def call(self,inputs):
        ''' The input is given in the form
       
        y = (q_1, q_2, \dot{q}_1,\dot{q}_2)
       
        '''
        y = tf.unstack(inputs,axis=1)
        q1 = y[0]
        q1dot = y[2]
        q2dot = y[3]
        return -q1+0.5*(tf.multiply(q1dot,q1dot)+tf.multiply(q2dot,q2dot))

class CentralForceLagrangian(tf.keras.layers.Layer):
    ''' Lagrangian for particle moving under a 1/r central force
    
    L = 1/r + 1/2*(\dot{r}^2 + r^2*\dot{\theta}^2)
      = 1/q_1 + 1/2*(\dot{q}_1^2 + q_1^2*\dot{q}_2^2)

    '''
    def __init__(self):
        super().__init__(self)

    def call(self,inputs):
        ''' The input is given in the form
       
        y = (q_1, q_2, \dot{q}_1,\dot{q}_2)
          = (r, \theta, \dot{r}, \dot{\theta})
       
        '''
        y = tf.unstack(inputs,axis=1)
        q1 = y[0]
        q1dot = y[2]
        q2dot = y[3]
        return 1/q1+0.5*(tf.multiply(q1dot,q1dot)+tf.multiply(q1,q1)*tf.multiply(q2dot,q2dot))
    
class fODE(tf.keras.layers.Layer):
    '''

    The time evolution of the variable y = (q_1, q_2, \dot{q}_1,\dot{q}_2)
    can be written as

    dy/dt = f(y)

    This class implement the function f for a given Lagrangian L. 

    In this case the function can be written as

    f = (\dot{q}_1, \dot{q}_2, g_1(y), g_2(y))

    with 

    g_k = ( (dL/dq)_j - \dot{q}_i*(J_{q,\dot{q}})_{ij} )*(J_{\dot{q},\dot{q}}^{-1})_{jk}

    and the matrices (J_{q,\dot{q}})_{ij} = d^2 L / (dq_i d\dot{q}_j),
    (J_{\dot{q},\dot{q}})_{ij} = d^2 L / (d\dot{q}_i d\dot{q}_j)

    '''
    def __init__(self,lagrangian):
        super().__init__(self)
        self.lagrangian = lagrangian

    @tf.function
    def _dd_y_jk(self,y,j,k):
        '''Helper function for computing Hessian d^2 L / (dy_j dy_k)
        '''
        d_y_j = tf.unstack(tf.gradients(self.lagrangian(y),y)[0],axis=1)[j]
        return tf.unstack(tf.gradients(d_y_j,y)[0],axis=1)[k]

    @tf.function
    def div_L(self,y):
        ''' Gradient of Lagrangian dL/dq'''
        dL = tf.unstack(tf.gradients(self.lagrangian(y),y)[0],axis=1)
        return tf.stack([dL[0],dL[1]],axis=1)
            
    @tf.function
    def J_qdotqdot(self,y):
        '''2x2 matrix J_{\dot{q},\dot{q}}'''
        line1 = tf.stack([self._dd_y_jk(y,2,2),self._dd_y_jk(y,2,3)],axis=1)
        line2 = tf.stack([self._dd_y_jk(y,3,2),self._dd_y_jk(y,3,3)],axis=1)
        return tf.stack([line1,line2],axis=1)

    @tf.function
    def J_qqdot(self,y):
        '''2x2 matrix J_{q,\dot{q}}'''
        line1 = tf.stack([self._dd_y_jk(y,0,2),self._dd_y_jk(y,0,3)],axis=1)
        line2 = tf.stack([self._dd_y_jk(y,1,2),self._dd_y_jk(y,1,3)],axis=1)
        return tf.stack([line1,line2],axis=1)

    @tf.function
    def J_qdotqdot_inv(self,y):
        '''2x2 matrix (J_{\dot{q},\dot{q}})^{-1}'''
        return tf.linalg.inv(self.J_qdotqdot(y))

    def call(self, inputs):
        '''Return value of the function f for a given input y'''
        y = tf.unstack(inputs,axis=1)
        qdot = tf.stack([y[2],y[3]],axis=1)
        qdotdot = tf.einsum("ai,aij->aj",
                            self.div_L(inputs) - tf.einsum(
                                "ai,aij->aj",
                                qdot,
                                self.J_qqdot(inputs)
                            ),
                            self.J_qdotqdot_inv(inputs)
        )
        return tf.concat([qdot,qdotdot],axis=1)

x = tf.constant([[1.0,2.0,3.0,4.0],[5.0,6.0,7.0,8.0],[9.,10.,11.,12.]])

lagrangian = CentralForceLagrangian()



lnn = fODE(lagrangian)

y = lnn(x)

h = 0.01

rk4 = RK4(lnn,h)

q0_t = tf.constant([1.0,1.0,0.0,0.4,10.0],shape=(1,5))
q = rk4(q0_t,record_trajectories=True)
R = rk4.y_traj[0,0,:]
Theta = rk4.y_traj[0,1,:]
print (R)
plt.clf()
ax = plt.gca()
ax.set_aspect('equal')
x_max = 1.0
ax.set_xlim(-x_max,+x_max)
ax.set_ylim(-x_max,+x_max)
plt.plot(R*np.cos(Theta),R*np.sin(Theta),
         linewidth=2,
         color='blue')
plt.savefig('planetary.pdf',bbox_inches='tight')
