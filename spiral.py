import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf
from tensorflow import keras
import sys
from timestepper import *

'''
========================================================================
 
Learning the parameters of the damped harmonic oscillator from data

========================================================================

Consider the damped harmonic oscillator

d^2x/dt^2 + \mu*dx/dt + \gamma*x = 0   (*)

with initial condition x(0)=x_0, dx/dt(0)=v_0
where \mu = \omega^2 + 1/4*\gamma^2.

Note that (*) can be rewritten as a first order system 

dy/dt = f_\theta(y) with y = (x,dx/dt)^T and
f_\theta(y) = (y_2, -\gamma*y_1 - \mu*y_2)

for which the exact solution is known (see below).

In this script we learn the parameters \theta=(\omega,\gamma) from data 

((x_0^j,v_0^j,t^j), (x(t^j)+\epsilon_x^j,dx/dt(t^t)+\epsilon_v^j))

where j=1,...,N and \epsilon_x^j ~ \epsilon_v^j ~ Normal(0,\sigma).
'''

tf.keras.backend.set_floatx('float64')

class DampedHOExact(object):
    '''Exact solver for damped harmonic oscillator.

    This can be used to generate artificial data

    :arg omega: angular frequency \omega
    :arg gamma: damping parameter \gamma
    '''
    def __init__(self,omega,gamma):
        self.omega = omega
        self.gamma = gamma
        self.mu = self.omega**2+0.25*self.gamma**2

    def y_exact(self,x0,v0,t):
        '''Return exact solution for given starting value and time t, namely
        (x(t),dx/dt(t))

        This is given by:

        x(t) = (x_0*cos(\omega*t)+B*sin(\omega*t))*exp(-0.5*\self.gamma*t)
        dx/dt(t) = [ (B*\omega-x_0*0.5*\gamma)*cos(\omega*t) 
                   - (B*0.5*\gamma+x_0*\omega)*sin(\omega*t) ]
                 * exp(-0.5*\gamma*t)

          B = (v_0+0.5*\gamma*x_0)/\omega

        :arg x0: Initial position
        :arg v0: Initial velocity
        :arg t: integration time        
        '''
        B = (v0+0.5*self.gamma*x0)/self.omega
        x = (x0*np.cos(self.omega*t)+B*np.sin(self.omega*t))*np.exp(-0.5*self.gamma*t)
        v = ((B*self.omega-x0*0.5*self.gamma)*np.cos(self.omega*t)-(B*0.5*self.gamma+x0*self.omega)*np.sin(self.omega*t))*np.exp(-0.5*self.gamma*t)
        return (x,v)


    def generate_data(self,N,sigma=0.1):
        '''Generate N data points of the form

        (X_0, V_0, t_stop), (X(t), V(t)) 
        = (x_0^j,v_0^j,t^j), (x(t^j)+\epsilon_x^j,dx/dt(t^t)+\epsilon_v^j)

        with \epsilon_x^j ~ \epsilon_v^j ~ Normal(0,\sigma).

        here X_0 and V_0 are drawn uniformly from [-1.0,1.0] and 
        t_stop is drawn uniformly from [1.0,10.0].
        
        '''
        X0 = np.random.uniform(low=-1.0,high=1.0,size=N)
        V0 = np.random.uniform(low=-1.0,high=1.0,size=N)
        t_stop = np.random.uniform(low=1.0,high=10.0,size=N)
        X, V = self.y_exact(X0,V0,t_stop)
        x_data = np.zeros((N,3))
        x_data[:,0] = X0
        x_data[:,1] = V0
        x_data[:,2] = t_stop
        y_data = np.zeros((N,2))
        y_data[:,0] = X
        y_data[:,1] = V
        noise = np.random.normal(scale=sigma,size=(N,2))
        return x_data, y_data+noise
    
class F_DampedHO(keras.layers.Layer):
    ''' Function f_{\theta}(y) arising in the ODE formulation

    dy/dt = f_{\theta}(y)

    with f_{\theta}(y) = [y_1, -\theta_0*y_0 - \theta_1*y_1]

    Here \theta=(\omega,\gamma) is the parameter we want to learn
    
    '''
    def __init__(self):
        super(F_DampedHO, self).__init__()
        
    def build(self,input_shape):
        '''Construct parameters'''
        self.omega = self.add_weight(initializer="random_normal",
                                     trainable=True,
                                     name="omega")
        self.gamma = self.add_weight(initializer="random_normal",
                                     trainable=True,
                                     name="gamma")
        
    def call(self,inputs):
        '''Given y = (x,y) return (y,-\mu*x-\gamma*v) where
        \mu = \omega^2 + 1/4*\gamma^2

        :arg inputs: Current position and velocity (x,v)
        '''
        x, v = tf.unstack(inputs,axis=1)
        Fx =-(self.omega**2+0.25*self.gamma**2)*x - self.gamma*v
        return tf.stack([v,Fx],axis=1)
    
# Real model parameters
omega = 4.0
gamma = 0.5

# Gaussian noise
sigma = 0.05

# construct ODE model
spiral = DampedHOExact(omega,gamma)

# Number of data points
N = 8192

# Generate data
x_train, y_train = spiral.generate_data(N,sigma=sigma)

batch_size = 64

train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_dataset = train_dataset.shuffle(N).batch(batch_size)

# Construct ODE solver
f_dampedho = F_DampedHO()
h = 0.025 # Timestep size
model = RK4(f_dampedho,h)

optimizer = tf.keras.optimizers.RMSprop(learning_rate=0.1)
loss_fn = tf.keras.losses.MeanSquaredError()

epochs = 4

# Train over number of epochs
for epoch in range(epochs):
    print ("\nStart of epoch %d" % (epoch,))
    for step, (x_batch_train, y_batch_train) in enumerate(train_dataset):
        with tf.GradientTape() as tape:
            y_batch_pred = model(x_batch_train)
            loss_value = loss_fn(y_batch_pred, y_batch_train)
        grads = tape.gradient(loss_value, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))

        print(
            "Training loss (for one batch) at step %4d: %.4f"
            % (step, float(loss_value)))
        print ('  omega = %6.2f' % f_dampedho.omega.numpy(),
               '  gamma = %6.2f' % f_dampedho.gamma.numpy())

print ()
print (' omega [true]    = %4.2f' % omega)
print (' omega [learned] = %4.2f' % f_dampedho.omega.numpy())
print (' gamma [true]    = %4.2f' % gamma)
print (' gamma [learned] = %4.2f' % f_dampedho.gamma.numpy())

# Plot test data
N_test = 4
x_test, y_test = spiral.generate_data(N_test,sigma=sigma)

y_batch_pred = model(x_test,record_trajectories=True)
plt.clf()
for k in range(N_test):
    rho = k/N_test
    color=(rho,0,1-rho)
    plt.plot(model.y_traj[k,0,:],model.y_traj[k,1,:],
             linewidth=2,
             color=color)
    plt.plot(model.y_traj[k,0,-1],model.y_traj[k,1,-1],
             marker='o',
             markersize=4,
             markerfacecolor='white',
             markeredgecolor=color,
             markeredgewidth=2,
             color=color)
    x_exact, v_exact = spiral.y_exact(x_test[k,0],x_test[k,1],x_test[k,2])
    plt.plot(x_exact,v_exact,
             marker='*',
             markersize=4,
             markerfacecolor=color,
             markeredgecolor=color,
             markeredgewidth=2,
             color=color)

ax = plt.gca()
ax.set_xlabel('$x(t)$')
ax.set_ylabel('$v(t)$')
plt.savefig('trajectories.pdf',bbox_inches='tight')
