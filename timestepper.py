import numpy as np
import tensorflow as tf
from tensorflow import keras

class RK4(keras.layers.Layer):
    ''' Fourth order Runge Kutta integrator

    Solves the ODE dy/dt = f_{\theta}(y) with a RK4 integrator
    and timestep size h.

    :arg f_layer: Function f_{\theta} to integrate
    :arg h: timestep size

    '''
    def __init__(self,f_layer,h):
        super(RK4, self).__init__()
        self.f_layer = f_layer
        self.h = h

    def call(self,inputs,record_trajectories=False):
        '''
        Calculate and return y(t) for a given input (y_0,t).

        :arg inputs: Input (y_0,t)
        '''

        # Number of samples
        N = inputs.get_shape().as_list()[0]
        # Dimension of input vector y
        dim = inputs.get_shape().as_list()[1]-1
        
        # Extract initial conditions and stopping times
        yt = tf.unstack(inputs,axis=1)
        t_stop = yt[-1]
        # Set initial condition for RK4 integrator
        y_hat = tf.stack(yt[0:dim],axis=1)
        # Find largest stopping time
        t_max = tf.math.reduce_max(t_stop)
        if (record_trajectories):
            # Number of timesteps
            M = int(np.floor((t_max+0.5*self.h)/self.h))
            self.y_traj = np.zeros((N,dim,M+1))
            self.y_traj[:,:,0] = y_hat.numpy()[:,:]
        t = 0.0
        k_step = 1
        # Integrate
        while(t < t_max-0.5*self.h):
            k1 = self.f_layer(y_hat)
            k2 = self.f_layer(y_hat+0.5*self.h*k1)
            k3 = self.f_layer(y_hat+0.5*self.h*k2)
            k4 = self.f_layer(y_hat+self.h*k3)
            # Mask out (i.e. do not increment) any data which is beyond the
            # stopping time
            s_mask = tf.stack([tf.cast(t<t_stop-0.5*self.h,tf.float64)],axis=1)
            mask = tf.repeat(s_mask,repeats=dim,axis=1)
            y_hat += self.h/6.0*tf.math.multiply(k1+2*k2+2*k3+k4,mask)
            if (record_trajectories):
                self.y_traj[:,:,k_step] = y_hat.numpy()[:,:]
            t += self.h
            k_step += 1
        return y_hat
